
// target the elements
const registerForm = document.getElementById('register');

// element.addEventListener("event", () => {})
registerForm.addEventListener("submit", (e) => {
	e.preventDefault()	//prevent from refereshing the browser

	// get the values of the input fields
	const fn = document.getElementById('firstname').value;
	const ln = document.getElementById('lastname').value;
	const email = document.getElementById('email').value;
	const pw = document.getElementById('password').value;
	const cpw = document.getElementById('cpw').value;


	//condition pw should match confirm pw
	if(pw === cpw){

		// send the request to the server
			// fetch(URL, {options})
		fetch(`http://localhost:3008/api/users/email-exists`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: fn,
				lastName: ln,
				email: email,
				password: pw
			})
		})
		.then( result => result.json())
		.then(result => {
			// console.log(result)	//boolean
			if(result == false){
				// if email not existing in the DB, send a request to register the user
				fetch(`http://localhost:3008/api/users/register`, {
					method: "POST",
					headers:{
						"Content-Type": "application/json",
					},
					body: JSON.stringify({
						//user input
						firstName: fn,
						lastName: ln,
						email: email,
						password: pw
					})
				})
				.then(result => result.json())
				.then(result => {
					// console.log(result)	//boolean
					if(result){
						alert('User successfully registered!')

						window.location.replace('./login.html')
					} else {
						alert(`Please try again`)
					}
				})

			} else {
				alert(`User already exists`)
			}
		})

	}
})


