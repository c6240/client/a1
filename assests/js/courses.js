const token = localStorage.getItem('token')
const isAdmin = localStorage.getItem("admin")
const courseContainer = document.getElementById('anyContainer')
const adminButton = document.getElementById('adminButton')
let courses;


// console.log(isAdmin == false)
// console.log(typeof isAdmin)
// console.log(typeof "false")

if(isAdmin == "false"){
	//regular user

	//send request to get all active courses
	fetch(`http://localhost:3008/api/courses/isActive`, {
		method: "GET",
		headers:{
			"Authorization": `Bearer ${token}`
		}
	})
	//wait for server's response
	.then(result => result.json())
	.then(result => {
		// console.log(result)

		//display the courses using card
		if(result.length < 1){
			return `No Courses Available`

		} else {
			// if array is not empty, use map method to return a card for each element
			courses = result.map(course => {
				// console.log(course)
				const {courseName, description, price} = course

				return(
					`
						<div class="col-12 col-md-4 my-2">
							<div class="card" style="width: 18rem;">
								<div class="card-body">
									<h5 class="card-title">
										${courseName}
									</h5>
									<p class="card-text text-left">
										${description}
									</p>
									<p class="card-text text-right">
										${price}
									</p>
									<a 
										class="btn btn-info"
										href="./specificCourse.html?courseId=">
										Select Course
									</a>
								</div>
							</div>
						</div>
					`
				)
			}).join(" ")

			// console.log(courses)

			courseContainer.innerHTML = courses
		}
	})

} else {
	//admin
	adminButton.innerHTML =
	//show the button if it is Admin
	`
		<a class="btn btn-info m-3" href="./createCourse.html">Add Course</a>
		<a class="btn btn-info m-3" href="./dashboard.html">Dashboard</a>

	`

	//show all the courses in the Courses Page 
		// both offered and not offered
	fetch(`http://localhost:3008/api/courses`, {
		method: "GET",
		headers:{
			"Authorization": `Bearer ${token}`
		}
	})
	//wait for server's response
	.then(result => result.json())
	.then(result => {
		// console.log(result)

		//display the courses using card
		if(result.length < 1){
			return `No Courses Available`

		} else {
			// if array is not empty, use map method to return a card for each element
			courses = result.map(course => {
				// console.log(course)
				const {courseName, description, price} = course

				return(
					`
						<div class="col-12 col-md-4 my-2">
							<div class="card" style="width: 18rem;">
								<div class="card-body">
									<h5 class="card-title">
										${courseName}
									</h5>
									<p class="card-text text-left">
										${description}
									</p>
									<p class="card-text text-right">
										${price}
									</p>
									<div class="card-footer">
									
									</div>
								</div>
							</div>
						</div>
					`
				)
			}).join(" ")

			// console.log(courses)

			courseContainer.innerHTML = courses
		}
})
}
