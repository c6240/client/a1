
const token = localStorage.getItem('token')

const createForm = document.getElementById('createCourse')

addEventListener("submit", (e) => {
	e.preventDefault()

	let cn = document.getElementById('courseName').value
	let description = document.getElementById('description').value
	let price = document.getElementById('price').value

	fetch(`http://localhost:3008/api/courses/create`, {
		method: "POST",
		headers:{
			"Content-Type": "application/json",
			"Authorization": `Bearer ${token}`
		},
		body: JSON.stringify({
			courseName: cn,
			description: description,
			price: price
		})
	})
	.then(result => result.json())
	.then(result => {
		console.log(result)

		if (result) {
			alert('Course successfully added')

			window.location.replace('./course.html')
		} else {
			alert('Something went wrong')
		}
	})
})